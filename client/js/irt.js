/*----------------------------------------------------------
Fichero: irt.js
Aquí se gestíona todo el contenido de InfoRutasTenerife
----------------------------------------------------------*/


$( document ).on( "pagecreate", "#map-page", function() {

      var serverUrl = 'http://www.moiseslodeiro.es/irt/getirt.php';
      //var serverUrl = '../server/getirt.php';


    //-----------------------------------------------------
    // Variables internas NO TOCAR SI NO SABES LO QUE HACES      

      var defaultLat = 28.4814628;
      var defaultLan = -16.3229073;

      var map = null;
      var directionsDisplay;
      var boxpolys = null;
      var directions = null;
      var routeBoxer = null;
      var distance = null; // km
      //var weatherLayer = null;
      var routes = [];
      var routesPin = []; // Array de marcadores de ruta
      var gasolineras = null;
      var circle = null;
      var defaultLatLng = new google.maps.LatLng( defaultLat , defaultLan );
      var infowindow = new google.maps.InfoWindow();
      var directionsDisplay = null;
      var info = null;
      var geolocalizar = true;
      var retLat = null;
      var retLan = null;
      //----------------------------------------
      var marker;
      var markersArray = [];
      var markerCamera;
      var markerCameraArray = [];
      var markerCartel;
      var markerCartelArray = [];
      var markerDensidad;
      var markerDensidadArray = [];
      var markerIncidences;
      var markerIncidencesArray = [];
      //----------------------------------------
      var show95  = true;
      var show95p = true;
      var show98  = true;
      var showGasoilA  = true;
      var showGasoilN  = true;
      var showLicuados = true;
      //----------------------------------------
      var showDisa = true;
      var showRepsol = true;
      var showTgas = true;
      var showShell = true;
      var showBp = true;
      var showPcan = true;
      var showCepsa = true;
      var showTexaco = true;
      var showOtras = true;
      //----------------------------------------
      //var config = [];

//-------------------------- INICIALIZACIÓN -----------------------//




    $.blockUI({
    
      fadeIn: 0, 
      css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
        opacity: .6, 
        color: '#fff' 
      },
      message: '<img src="images/ajax-loader.gif" /><br />Cargando Datos Nuevos',
      onBlock: function(){


        if( localStorage.getItem('csvData') == undefined ){
          loadData();

        }else{
          getNewData();
        }

      }
    
    });
    $.unblockUI();


    

      var opcionesMapa = {
        zoom: 10,
        maxZoom: 16,
        center: defaultLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
      };

      var map = new google.maps.Map( document.getElementById("map") , opcionesMapa );
      google.maps.event.trigger( map , 'resize' );
   
      routeBoxer = new RouteBoxer();
        
      directionService = new google.maps.DirectionsService();
      directionsRenderer = new google.maps.DirectionsRenderer({ map: map , suppressMarkers : true }); 

      var noPoi = [
      {
          featureType: "poi",
          stylers: [
            { visibility: "off" }
          ]   
        }
      ];

      map.setOptions({styles: noPoi});

    /*weatherLayer = new google.maps.weather.WeatherLayer({
      temperatureUnits: google.maps.weather.TemperatureUnit.CELCIUS
    });
    weatherLayer.setMap(map);

    var cloudLayer = new google.maps.weather.CloudLayer();
    cloudLayer.setMap(map);
    */

    tmp = JSON.parse(localStorage.csvData);
    f = new Date( tmp.info.updatedOn*1000 );
    $("#fechaActualizacion").html( f.getDate()+"/"+(f.getMonth()+1)+"/"+f.getFullYear() );




google.maps.event.addDomListener(window, 'load', cargar);
function cargar(){

    if( localStorage.getItem("configuracion") != undefined )
    {
      
      config = JSON.parse(localStorage.getItem( "configuracion" ));

      $("#isla").val(config['isla']);
      $("#toggleDestacarBarata").prop( 'checked' , config['toggleDestacarBarata'] );

      if( config['toggleDestacarBarata'] == false ){
        $(".visiblegasolina").hide();
      }


      $("#filterby").val( config['filterby'] );

      $("#checkdisa").prop( 'checked' , config['checkdisa'] );
      $("#checkrepsol").prop( 'checked' , config['checkrepsol'] );
      $("#checktgas").prop( 'checked' , config['checktgas'] );
      $("#checkshell").prop( 'checked' , config['checkshell'] );
      $("#checkbp").prop( 'checked' , config['checkbp'] );
      $("#checkpcan").prop( 'checked' , config['checkpcan'] );
      $("#checkcepsa").prop( 'checked' , config['checkcepsa'] );
      $("#checktexaco").prop( 'checked' , config['checktexaco'] );
      $("#checkotras").prop( 'checked' , config['checkotras'] );



      showDisa = config['checkdisa'];
      showRepsol = config['checkrepsol'];
      showTgas = config['checktgas'];
      showShell = config['checkshell'];
      showBp = config['checkbp'];
      showPcan = config['checkpcan'];
      showCepsa = config['checkcepsa'];
      showTexaco = config['checktexaco'];
      showOtras = config['checkotras'];
      geolocalizar = config['geolocalizar'];


      $("#toggleGeolocalizacion").prop('checked',geolocalizar);

      $("#desvio").val( config['desvio'] );
      $("#showradius").html( config['desvio'] );

      retLan = 0;
      retLat = 0;

      switch( config['isla'] ){
        case 'tenerife' : retLan = 28.4814628; retLat = -16.3229073; changeCoords = true; cleanMap(); $("#toggleIncidences").show(); $("#toggleCamera").show(); $("#toggleDensidad").show(); $("#toggleCartel").show(); break;
        case 'lagomera' : retLan = 28.115616; retLat = -17.2413293; changeCoords = true; cleanMap(); $("#toggleIncidences").hide(); $("#toggleCamera").hide(); $("#toggleDensidad").hide(); $("#toggleCartel").hide(); break;
        case 'lapalma'  : retLan = 28.67007; retLat = -17.8428602; changeCoords = true; cleanMap(); $("#toggleIncidences").hide();$("#toggleCamera").hide(); $("#toggleDensidad").hide(); $("#toggleCartel").hide();break;
        case 'elhierro' : retLan = 27.7272371; retLat = -17.9934224; changeCoords = true; cleanMap(); $("#toggleIncidences").hide(); $("#toggleCamera").hide(); $("#toggleDensidad").hide(); $("#toggleCartel").hide(); break;
      }

      changeCoords = true;

      map.panTo( new google.maps.LatLng( retLan , retLat ) );


    /*var opcionesMapa = {
      zoom: 10,
      maxZoom: 16,
      center: new google.maps.LatLng( retLan , retLat ),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    };*/

    //var map = new google.maps.Map( document.getElementById("map") , opcionesMapa );
 
    /*var routeBoxer = new RouteBoxer();
      
    var directionService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer({ map: map , suppressMarkers : true }); 

    var noPoi = [
    {
        featureType: "poi",
        stylers: [
          { visibility: "off" }
        ]   
      }
    ];

    map.setOptions({styles: noPoi});*/



}

} // fin sexy



//-------------------------- EVENTOS -------------------------------//

  $("#checkdisa").click(function(){ if( $(this).is( ':checked' ) ){ showDisa = 1; }else{ showDisa = 0; } guardarCambios(); });
  $("#checkrepsol").click(function(){ if( $(this).is( ':checked' ) ){ showRepsol = 1; }else{ showRepsol = 0; } guardarCambios(); });
  $("#checktgas").click(function(){ if( $(this).is( ':checked' ) ){ showTgas = 1; }else{ showTgas = 0; } guardarCambios(); });
  $("#checkshell").click(function(){ if( $(this).is( ':checked' ) ){ showShell = 1; }else{ showShell = 0; } guardarCambios(); });
  $("#checkbp").click(function(){ if( $(this).is( ':checked' ) ){ showBp = 1; }else{ showBp = 0; } guardarCambios(); });
  $("#checkpcan").click(function(){ if( $(this).is( ':checked' ) ){ showPcan = 1; }else{ showPcan = 0; } guardarCambios(); });
  $("#checkcepsa").click(function(){ if( $(this).is( ':checked' ) ){ showCepsa = 1; }else{ showCepsa = 0; } guardarCambios(); });
  $("#checktexaco").click(function(){ if( $(this).is( ':checked' ) ){ showTexaco = 1; }else{ showTexaco = 0; } guardarCambios(); });
  $("#checkotras").click(function(){ if( $(this).is( ':checked' ) ){ showOtras = 1; }else{ showOtras = 0; } guardarCambios(); });
  
  //----------------------------------------

  

  function guardarCambios(){

    var config = {};

    config['isla'] = $("#isla").val(); // !!!!! cambiar cuando se haga el load para la isla
    config['toggleDestacarBarata'] = $("#toggleDestacarBarata").is(":checked");
    config['filterby'] = $("#filterby").val();

    config['checkdisa'] = $("#checkdisa").is(":checked");
    config['checkrepsol'] = $("#checkrepsol").is(":checked");
    config['checktgas'] = $("#checktgas").is(":checked");
    config['checkshell'] = $("#checkshell").is(":checked");
    config['checkbp'] = $("#checkbp").is(":checked");
    config['checkpcan'] = $("#checkpcan").is(":checked");
    config['checkcepsa'] = $("#checkcepsa").is(":checked");
    config['checktexaco'] = $("#checktexaco").is(":checked");
    config['checkotras'] = $("#checkotras").is(":checked");

    config['desvio'] = $("#desvio").val();
    config['geolocalizar'] = $("#toggleGeolocalizacion").is(":checked");

    localStorage.setItem( "configuracion" , JSON.stringify( config ) );


  }


  $("#limpiar").click(function(){ 


    $.blockUI({
    
      fadeIn: 0, 
      css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
        opacity: .6, 
        color: '#fff' 
      },
      message: '<img src="images/ajax-loader.gif" /><br />Limpiando...',
      onBlock: function(){

        cleanMap();
        $("#limpiar").addClass("ui-disabled");

      }
    
    });
    $.unblockUI();

  });
  
  //----------------------------------------

  $("#showradius").html($("#desvio").val());

  //----------------------------------------

  $("#desvio").change(function(){
    $("#showradius").html($(this).val());
    guardarCambios();
  });

  //----------------------------------------

  $("#toggleDestacarBarata").click(function(){

    $(".visiblegasolina").toggle();
    guardarCambios();

  });

  //----------------------------------------

  $("#cercanas").click(function(){

    $.blockUI({
    
      fadeIn: 0, 
      css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
        opacity: .6, 
        color: '#fff' 
      },
      message: '<img src="images/ajax-loader.gif" /><br />Buscando Posición',
      onBlock: function(){

          if( routes.length == 1 )
              getCercanas();
          else{
            
            if( geolocalizar == 1 ){
            
              if( navigator.geolocation ){


                $("#limpiar").removeClass('ui-disabled');
                navigator.geolocation.getCurrentPosition(function( position ){

                  getCercanas( new google.maps.LatLng( position.coords.latitude , position.coords.longitude ));

                }, function(){ alert("Error\nPuede deberse a que la señal de su GPS es muy floja o bien que no tiene activada la localización."); $.unblockUI(); } , { maximumAge: 3000 , timeout: 50000, enableHighAccuracy: true } );

              } else {
                alert("No es posible geolocalizarlo en este momento.");
              }

            }else{
              alert("Debe poner al menos una localización");
            }

          }
      }
    
    });
    $.unblockUI();
    
  });

  //----------------------------------------

  function showConfirm() {
    navigator.notification.confirm(
      '¿Quiere cerrar la aplicación?',
      exitFromApp,              
      'Salir',            
      'Cancelar,Sí'       
    );
  }
 
  //---------------------------------------- 

  function exitFromApp(buttonIndex) {
    if( buttonIndex==2 && $.mobile.activePage.is("#map-page") ){
      navigator.app.exitApp();
    }
  }

  //----------------------------------------

  $("#gasolineras").click(function(){

    directionsRenderer.setMap(null);
    directionService = new google.maps.DirectionsService();
    directionsRenderer = new google.maps.DirectionsRenderer({ map: map , suppressMarkers : true });

    if( routes.length >= 2 )
      route();
    else
      alert("Debes poner al menos dos o más localizaciones haciendo click en el mapa indicando al menos un punto de inicio y otro de fin de ruta.");

    $.unblockUI();

  });
  
  //----------------------------------------
  
  $("#toggle95").click( function(){

    if( $("#toggle95").is( ':checked') )
      show95 = 1;
    else
      show95 = 0;

    guardarCambios();

  });

  //----------------------------------------

  $("#toggle98").click(function(){

    if( $("#toggle98").is( ':checked') )
      show98 = 1;
    else
      show98 = 0;

    guardarCambios();

  });

  //----------------------------------------

  $("#toggleGA").click(function(){

    if( $("#toggleGA").is( ':checked') )
      showGasoilA = 1;
    else
      showGasoilA = 0;

    guardarCambios();

  });

  //----------------------------------------

  $("#toggle95p").click(function(){

    if( $("#toggle95p").is( ':checked') )
      show95p = 1;
    else
      show95p = 0;

    guardarCambios();

  });

  //----------------------------------------

  $("#toggleLicuados").click(function(){

    if( $("#toggleLicuados").is( ':checked') )
      showLicuados = 1;
    else
      showLicuados = 0;

    guardarCambios();

  });  

  //----------------------------------------

  $("#toggleGN").click(function(){

    if( $("#toggleGN").is( ':checked') )
      showGasoilN = 1;
    else
      showGasoilN = 0;

    guardarCambios();

  });

  //----------------------------------------

  $("#toggleGeolocalizacion").click( function(){

    if( $("#toggleGeolocalizacion").is( ':checked') )
      geolocalizar = 1;
    else
      geolocalizar = 0;


    guardarCambios();

    //console.log(geolocalizar);

  });

  //----------------------------------------

  $("#toggleCartel").click(function(){

    $.blockUI({
    
      fadeIn: 0, 
      css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
        opacity: .6, 
        color: '#fff' 
      },
      message: '<img src="images/ajax-loader.gif" /><br />Mostrando carteles',
      onBlock: function(){

        if( $("#toggleCartel").is( ':checked') ){

          showCartelInfo();
          $("#limpiar").removeClass("ui-disabled");
          

        }else{

          if( markerCartelArray != null ){
            for (var i = 0; i < markerCartelArray.length; i++ ) {
              markerCartelArray[i].setMap(null);
            }
            markerCartelArray.length = 0;
          }


        }

      }
    
    });
    $.unblockUI();

  });

  //----------------------------------------


  $("#toggleIncidences").click(function(){


    $.blockUI({
    
      fadeIn: 0, 
      css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
        opacity: .6, 
        color: '#fff' 
      },
      message: '<img src="images/ajax-loader.gif" /><br />Mostrando Incidencias',
      onBlock: function(){

        if( $( "#toggleIncidences" ).is( ':checked') ){

          showIncidences();
          $("#limpiar").removeClass("ui-disabled");

        }else{

            if( markerIncidencesArray != null ){
              for (var i = 0; i < markerIncidencesArray.length; i++ ) {
                markerIncidencesArray[i].setMap(null);
              }
              markerIncidencesArray.length = 0;
            }

        }

      }
    
    });
    $.unblockUI();

  });



  //----------------------------------------


  $("#toggleDensidad").click(function(){


    $.blockUI({
    
      fadeIn: 0, 
      css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
        opacity: .6, 
        color: '#fff' 
      },
      message: '<img src="images/ajax-loader.gif" /><br />Mostrando densidad',
      onBlock: function(){

        if( $( "#toggleDensidad" ).is( ':checked') ){

          showRoadInfo();
          $("#limpiar").removeClass("ui-disabled");

        }else{

            if( markerDensidadArray != null ){
              for (var i = 0; i < markerDensidadArray.length; i++ ) {
                markerDensidadArray[i].setMap(null);
              }
              markerDensidadArray.length = 0;
            }

        }

      }
    
    });
    $.unblockUI();

  });

  //----------------------------------------

  $("#toggleCamera").click(function(){

    $.blockUI({
    
      fadeIn: 0, 
      css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
        opacity: .6, 
        color: '#fff' 
      },
      message: '<img src="images/ajax-loader.gif" /><br />Mostrando cámaras',
      onBlock: function(){

        if( $("#toggleCamera").is( ':checked') ){

          showCamarasInfo();
          $("#limpiar").removeClass("ui-disabled");

        }else{

          if( markerCameraArray != null ){
            for (var i = 0; i < markerCameraArray.length; i++ ) {
              markerCameraArray[i].setMap(null);
            }
            markerCameraArray.length = 0;
          }
        }


      }
    
    });
    $.unblockUI();

  });

  //----------------------------------------

  var changeCoords = false;
  $("#isla").change(function(){

    switch( $("#isla").val() ){
      case 'tenerife' : retLan = 28.4814628; retLat = -16.3229073; changeCoords = true; cleanMap(); $("#toggleCamera").show(); $("#toggleDensidad").show(); $("#toggleCartel").show(); break;
      case 'lagomera' : retLan = 28.115616; retLat = -17.2413293; changeCoords = true; cleanMap(); $("#toggleCamera").hide(); $("#toggleDensidad").hide(); $("#toggleCartel").hide(); break;
      case 'lapalma'  : retLan = 28.67007; retLat = -17.8428602; changeCoords = true; cleanMap(); $("#toggleCamera").hide(); $("#toggleDensidad").hide(); $("#toggleCartel").hide();break;
      case 'elhierro' : retLan = 27.7272371; retLat = -17.9934224; changeCoords = true; cleanMap(); $("#toggleCamera").hide(); $("#toggleDensidad").hide(); $("#toggleCartel").hide(); break;
    }

    guardarCambios();

  });

  //----------------------------------------

  $(document).on( 'pagebeforeshow' , '#map-page' ,function(event){



    if( map.center.B != undefined && changeCoords == false ){
      retLat = map.center.B;
      retLan = map.center.k;
    }

  });

  //----------------------------------------

  $( document ).bind( 'pageshow' , function( event, data ){

    if( $.mobile.activePage.is("#map-page") || $.mobile.activePage.is("#rutastext")  ){
      $('#map').append(map.getDiv());
      google.maps.event.trigger( map , 'resize' );
      map.panTo( new google.maps.LatLng( retLan , retLat ) );

      if( changeCoords )
        map.setZoom( 10 );

      changeCoords = false;
    }


  });

  //----------------------------------------

  google.maps.event.addListener( map , 'click' , function( event ){

    placeMarker( event.latLng );

    waypoint = {
      location: event.latLng,
      stopover: true
    };

    routes.push( waypoint );

  });

  //----------------------------------------

  google.maps.Circle.prototype.contains = function(latLng) {
    return this.getBounds().contains( latLng ) && google.maps.geometry.spherical.computeDistanceBetween( this.getCenter() , latLng ) <= this.getRadius();
  }

  //----------------------------------------

  var strictBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(27.753652,-16.1621475), new google.maps.LatLng(28.8280458,-18.1012347)       
  );

  //----------------------------------------

  $("#gasolineras").addClass('ui-disabled');
  $("#ayuda-button").addClass('ui-disabled');
  $("#cercanas").addClass('ui-disabled');
  $("#limpiar").addClass('ui-disabled');
  $("#configuracion").addClass('ui-disabled');

  //----------------------------------------

  $("#doTour").click(function(){
    $.mobile.changePage("#map-page");
    $.prompt(tourStates,{ loaded: function(){ $.prompt.goToState('state0'); }});
  });

  //----------------------------------------

  function tourSubmitFunc( e , v , m , f ){

    if(v === -1){
      $.prompt.prevState();
      return false;
    }else if(v === 1){
      $.prompt.nextState();
      return false;
    }else if( v === 0 ){
      $("#ayuda-button").removeClass('ui-disabled');
      desactivarMenu();
      localStorage.setItem('tour',1);
      $.prompt.close();
      return false;
    }
  
  }

  //----------------------------------------

  $("#ayudaCamaras").click(function(){
    $.prompt( tourStates , {
      loaded: function(){
        $.prompt.goToState( 'state10' );
      }
    });
  });

  //----------------------------------------

  $("#ayudaCarteles").click(function(){
    $.prompt( tourStates , {
      loaded: function(){
        $.prompt.goToState( 'state11' );
      }
    });
  });

  //----------------------------------------

  $("#ayudaDensidad").click(function(){
    $.prompt( tourStates , {
      loaded: function(){
        $.prompt.goToState( 'state12' );
      }
    });
  });

  //----------------------------------------

  $("#ayudaBaratas").click(function(){
    $.prompt( tourStates , {
      loaded: function(){
        $.prompt.goToState( 'state13' );
      }
    });
  });

  //----------------------------------------

  $("#ayudaCombustibles").click(function(){
    $.prompt( tourStates , {
      loaded: function(){
        $.prompt.goToState( 'state14' );
      }
    });
  });

  //----------------------------------------

  $("#ayudaGasolineras").click(function(){
    $.prompt( tourStates , {
      loaded: function(){
        $.prompt.goToState( 'state15' );
      }
    });
  });

  //----------------------------------------

  var tourStates =  {

    loading: {
      title: 'Cargando',
      buttons: {},
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: 'h1', x: 10, y: 60, width: 350 }, 
    },
    state0: {
      title: 'Bienvenido',
      html: 'Antes de empezar a utilizar InfoRutasTenerife sigue los pasos de este sencillo asistente de configuración. Este mensaje sólo aparecerá la primera vez que inicies la aplicación. Haz click en "Siguiente" para comenzar.',
      buttons: { 'Saltar' : 0 , 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: 'h1', x: 10, y: 60, width: 350 },
      submit: tourSubmitFunc
    },
    state1: {
      title: 'Primer Paso',
      html: 'En el mapa puedes insertar "pines" ( <img src="images/Pin.png" height="16" /> ). Estos son referencias de puntos en el mapa para localizar gasolineras cercanas o marcar puntos en una ruta. Si te equivocas poniendo uno, con hacer click sobre él, desaparecerá.',
      buttons: { 'Saltar' : 0 , 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: 'h1', x: 10, y: 60, width: 350 },
      submit: tourSubmitFunc
    },
    state2: {
      title: 'Al pulsar "En Ruta"...',
      html: 'Si insertas <b>más de un pin</b> ( 2, 3, ... ), se calcularán las gasolineras cercanas a la ruta dependiendo del radio que hayas puesto.',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      position: { container: '#gasolineras', x: 10, y: -220, width: 280, arrow: 'bl' },
      submit: tourSubmitFunc
    },
    state3: {
      title: 'Indicador de Radio',
      html: 'Esta cantidad representa el número de metros que puede haber de desvío en una ruta para localizar las gasolineras. Es la misma para localizar en un radio de un punto las gasolineras cercanas.',
      buttons: { 'Saltar' : 0 , 'Atrás': -1 , 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#showradius', x: -50, y: 30, width: 350 , arrow:  'tl' },
      submit: tourSubmitFunc
    },
    state4: {
      title: 'Al pulsar "Cercanas"...',
      html: 'Si <b>NO</b> has insertado <b>ningún pin ( <img src="images/Pin.png" height="16" /> )</b>, intentará buscar cerca de donde estés las gasolineras a menos del radio indicado. Para ésto deberás tener habiliada la localización en tu dispositivo móvil.',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#cercanas', x: -50, y: -250, width: 280, arrow: 'bl' },
      submit: tourSubmitFunc
    },
    state5: {
      title: 'Al pulsar "Cercanas"...',
      html: 'Si has insertado <b>un sólo pin ( <img src="images/Pin.png" height="16" /> )</b>, buscará partiendo del punto y en el radio indicado, las gasolineras cercanas a él.',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#cercanas', x: -50, y: -220, width: 280, arrow: 'bl' },
      submit: tourSubmitFunc
    },
    state6: {

      title: 'Al pulsar "Configuración"...',
      html: 'Te llevará al panel de configuración que veremos a continuación',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#cercanas', x: -50, y: -190, width: 280, arrow: 'br' },
      
      submit: function(e,v,m,f){

        e.preventDefault();
        if( v == 1 ){
          $.mobile.changePage( "#config-page" );
          $.prompt.nextState();
        }

      }

    },
    state7: {

      title: 'Configuración',
      html: 'Este es el panel de Configuración. Vamos a proceder a ver las configuraciones posibles.',
      buttons: { 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: 'h1', x: 10, y: 60, width: 350 },
      submit: function(e,v,m,f){

        e.preventDefault();
        
        if( v == 1 ){
          
          $.prompt.nextState();
        
        }else{

          if( v == -1 ){

            $.prompt.prevState();
            $.mobile.changePage( "#map-page" );
            window.setTimeout(function(){
              google.maps.event.trigger( map ,'resize');
              $("#map-page").css( "padding-bottom", "0px" );
            } , 800 );
          
            return false;

          }

        }

      }
      
    },  
    state8: {

      title: 'Metros de radio',
      html: 'Aquí puedes cambiar, <b>en metros</b>, la distancia máxima de la que te quieres desviar/mover para localizar una gasolinera.',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#desvio', x: -200, y: 50, width: 280, arrow: 'tr' },
      submit: tourSubmitFunc

    },
    state9: {
      html: 'Si marcas la casilla y haces click sobre "Cercanas" sin poner ningún pin, localizará las gasolineras cercanas a tu posición. ¡También te servirá para cuando hagas click sobre una gasolinera puedas marcar la ruta para llegar a ella!',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#toggleGeolocalizacion', x: -250, y: 40, width: 280, arrow: 'tr' },
      submit: tourSubmitFunc
    },
    state10: {
      html: 'Si marcas la casilla mostrará sobre el mapa las cámaras de seguridad de tráfico',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#toggleCamera', x: -250, y: 40, width: 280, arrow: 'tr' },
      submit: tourSubmitFunc
    },
    state11: {
      html: 'Si marcas la casilla mostrará los carteles informativos sobre el mapa.',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#toggleCartel', x: -250, y: 40, width: 280, arrow: 'tr' },
      submit: tourSubmitFunc
    },
    state12: {
      html: 'Si marcas la casilla mostrará información sobre densidad de tráfico sobre el mapa.',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#toggleDensidad', x: -250, y: 40, width: 280, arrow: 'tr' },
      submit: tourSubmitFunc
    },
    state13: {
      html: 'Al marcar esta casilla se mostrará con el icono de estrella amarillo la gasolinera que tenga el combustible más barato',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#toggleDestacarBarata', x: -250, y: 40, width: 280, arrow: 'tr' },
      submit: tourSubmitFunc
    },  
    /*state14: {
      title: 'Combustibles',
      html: 'Las siguientes 5 casillas ( Gasolina95, G95 Protección, 98, Diesel y Diesel+ ) pueden ser activadas/desactivadas. Si se activan mostrarán los precios de ese combustible encima de la gasolinera. ',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#combustiblescartel', x: 0, y: 40, width: 280, arrow: 'tl' },
      submit: tourSubmitFunc
    },*/
    state15: {
      title: 'Gasolineras',
      html: 'Aquí tienes un listado de gasolineras. Puedes activarlas o desactivarlas para que aparezcan en las búsquedas.',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#gasolinerascartel', x: 20, y: 40, width: 280, arrow: 'tl' },
      submit: tourSubmitFunc
    },
    state16: {
      title: 'Botón de Políticas',
      html: 'Si haces click en el botón de políticas podrás ver toda la información de la aplicación de un modo más detallado.',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Siguiente' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: '#fechaActualizacion', x: -200, y: -130, width: 280, arrow : 'br' },
      submit: tourSubmitFunc
    },
    state17: {
      title: '¡Gracias!',
      html: 'Ya has finalizado el pequeño tour por la aplicación. Espero que te sea de utilidad <img src="img/happy.png" />',
      buttons: { 'Saltar' : 0 , 'Atrás' : -1, 'Finalizar' : 1 },
      focus: -2,
      promptspeed: 'slow', show: 'show' , overlayspeed : 'slow',
      position: { container: 'h1', x: 10, y: 60, width: 280 },
      submit: function(e,v,m,f){

        e.preventDefault();
        
        if( v == 1 ){
          
          $.prompt.close();
          localStorage.setItem('tour','1');
          $.mobile.changePage('#map-page');
          $("#ayuda-button").removeClass('ui-disabled');
          desactivarMenu();

        }else{

          if( v == -1 ){

            $.prompt.prevState();
            return false;

          }

        }

      }

    }

  };

  //----------------------------------------

  if( localStorage.getItem('tour') != '1' )
    $.prompt(tourStates,{ loaded: function(){ $.prompt.goToState('state0'); }});
  else{
    localStorage.setItem('tour',1);
    $("#ayuda-button").removeClass('ui-disabled');
    desactivarMenu();
  }


//-------------------------- FUNCIONES ----------------------------//

  function preload(arrayOfImages) {
      $(arrayOfImages).each(function(){
          $('<img/>')[0].src = this;
      });
  }

  //----------------------------------------

  function getNewData(){
    
    gasolineras = JSON.parse( localStorage.getItem('csvData') );
    t = Math.floor( new Date().getTime() / 1000 );

    getupdate = 0;

    $.ajax({

      url: serverUrl+'?action=getupdate',
      global: true,
      async: false,
      type: "GET",         
      dataType: 'json',
      success: function( data )
      {            
        getupdate = data;
      }
    
    });

    if( (( t - gasolineras['info']['updatedOn'] ) >= ( 60*60*24 )) || ( getupdate > gasolineras['info']['updatedOn'] )){
      loadData();
    }
    gasolineras = gasolineras['data'];
  
  }

  //----------------------------------------

  function loadData(){
    
    $.ajax({

      url: serverUrl+'?action=gasolineras',
      global: true,
      async: false,
      type: "GET",         
      dataType: 'json',
      success: function( data )
      {            
        localStorage.setItem( "csvData" , JSON.stringify( data ) );
        info = data['info'];
        gasolineras = data['data'];


//console.log(gasolineras);


        preload([
            'images/ajax-loader.gif',
            'logos/bp.png',
            'logos/cepsa.png',
            'logos/disa.png',
            'logos/mejor.png',
            'logos/pcan.png',
            'logos/repsol.png',
            'logos/shell.png',
            'logos/texaco.png',
            'logos/tgas.png',
            'logos/unknown.png',
            'images/camera.png',
            'images/Pin.png',
            'images/road.png',
            'images/sign.png',
        ]);

      },
      error: function( data ){
        alert("Ha habido un error al cargar los datos. Reinicie la aplicación para solventar el problema");
        navigator.app.exitApp();
      } 
    });
    
  }

  //----------------------------------------

  function placeMarker( location )
  {


    if( $("#gasolineras").hasClass('ui-disabled') && routes.length >= 1 )
      $("#gasolineras").removeClass('ui-disabled');

    marker = new google.maps.Marker({
      position: location,
      map: map,
      icon: 'images/Pin.png'
    });

// TODO

    i = routes.length;

    google.maps.event.addListener( marker, 'click' , (function(marker, i) {
      return function() {
        this.setMap( null );
        routes.splice( i , 1 );
      }
    })( marker , i ));

    $("#limpiar").removeClass("ui-disabled");
    routesPin.push( marker );

  }

  //----------------------------------------

  function extend( from , to )
  {
      if( from == null || typeof from != "object" ) return from;
      if( from.constructor != Object && from.constructor != Array ) return from;
      if( from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
          from.constructor == String || from.constructor == Number || from.constructor == Boolean )
          return new from.constructor(from);

      to = to || new from.constructor();

      for (var name in from)
      {
          to[name] = typeof to[name] == "undefined" ? extend(from[name], null) : to[name];
      }

      return to;
  }

  //----------------------------------------

  $(".cerrarInstrucciones").click(function(){

    $("#directions-panel").hide();
    $("#info-gasolinera").hide();

    //infowindow.close();

  });

  $('body').on('click', 'button.comollegar', function() {

    $("#info-gasolinera").hide();
    $("#loadingPanel").show();
    $("#directions-panel-content").html('');
    $("#directions-panel").toggle();
    route($(this).attr('lat'),$(this).attr('lan'));

  });

  //----------------------------------------

  function route( lat , lan )
  {

    if( markersArray != null && lat == undefined ){
    
      for (var i = 0; i < markersArray.length; i++ ) {
        markersArray[i].setMap(null);
      }
      markersArray.length = 0;
    
    }
    clearBoxes();

    for( i = 0 ; i < routesPin.length ; i++ )
      routesPin[i].setMap(null);

    $("#gasolineras").addClass('ui-disabled');
    $("#configuracion").addClass('ui-disabled');

    if( isNaN( $("#desvio").val() ) )
      $("#desvio").val(500);

    distance = ( $("#desvio").val() / 1000 )*0.5;

    if( lat == undefined ){


      $.blockUI({
      
        fadeIn: 0, 
        css: { 
          border: 'none', 
          padding: '15px', 
          backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
          opacity: .6, 
          color: '#fff' 
        },
        message: '<img src="images/ajax-loader.gif" /><br />Calculando ruta...',
        onBlock: function(){
          
          routesCopy = extend( routes );
          routesCopy.splice(routes.length-1,1);
          routesCopy.splice(0,1);
          routesSize = routes.length-1;

          var request = {
            origin: new google.maps.LatLng( routes[0].location.lat() , routes[0].location.lng() ),
            destination: new google.maps.LatLng( routes[routesSize].location.lat() , routes[routesSize].location.lng() ),
            travelMode: google.maps.DirectionsTravelMode.DRIVING,
            waypoints: routesCopy
          }

          directionService.route(request, function(result, status) {
              
            if( status == google.maps.DirectionsStatus.OK ){

              directionsDisplay = directionsRenderer.setDirections(result);
              directionsRenderer.setPanel(document.getElementById("directions-panel-content"));

              var path = result.routes[0].overview_path;
              var boxes = routeBoxer.box(path, distance);

              drawBoxes(boxes);

            } else
              alert("Hay un error de cálculo.. Revisa que ninguno de los pines se salga de tierra ( ¡de momento las petrolíferas no recargan combustible! ).");

          });

          $.unblockUI();

        }
      
      });
      $.unblockUI();

      

    }else{


      $.blockUI({
      
        fadeIn: 0, 
        css: { 
          border: 'none', 
          padding: '15px', 
          backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px',
          opacity: .6, 
          color: '#fff' 
        },
        message: '<img src="images/ajax-loader.gif" /><br />Calculando Posición',
        onBlock: function(){

          navigator.geolocation.getCurrentPosition(function( position ){

            while( isNaN( position.coords.latitude ))
              continue;

            start = new google.maps.LatLng( position.coords.latitude , position.coords.longitude );
            end = new google.maps.LatLng( lat, lan );
            request = {
              origin: start,
              destination: end,
              travelMode: google.maps.DirectionsTravelMode.DRIVING 
            };

            directionService.route( request, function(result, status) {
              if( status == google.maps.DirectionsStatus.OK ) {
                directionsDisplay = directionsRenderer.setDirections(result);
                directionsRenderer.setPanel(document.getElementById("directions-panel"));
              }
            });
   
          }, function(){ alert("Error\nPuede deberse a que la señal de su GPS es muy floja o bien que no tiene activada la localización."); $.unblockUI(); } , { maximumAge: 3000 , timeout: 50000, enableHighAccuracy: true } );
          
          $.unblockUI();


        }
      
      });
      $.unblockUI();

    }


    routes = [];
    $("#loadingPanel").hide();

  }

  //----------------------------------------

  function drawBoxes(boxes) {

    boxpolys = new Array(boxes.length);

     	for ( var i = 0; i < boxes.length; i++ ) {

        boxpolys[i] = new google.maps.Rectangle({
  	      bounds: boxes[i],
  	      fillOpacity: 0.0,
  	      strokeOpacity: 0,
  	    	strokeColor: '#000000',
         	strokeWeight: 0,
         	map: map
        });
	        
	    }

      carburanteBarato = 99.99; // Este es el que le pongo a mi coche
      idCarburanteBarato = -1;
      campoCarburanteBarato = $("#filterby").val();

	    for( j = 0 ; j < boxes.length ; j++ ){
	      
        for( i = 0 ; i < gasolineras.length ; i++ ){


          if( gasolineras[i]['icon'] == 'disa.png' && showDisa == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'repsol.png' && showRepsol == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'tgas.png' && showTgas == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'shell.png' && showShell == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'bp.png' && showBp == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'pcan.png' && showPcan == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'cepsa.png' && showCepsa == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'texaco.png' && showTexaco == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'unknown.png' && showOtras == 0 )
            continue;
          

	        if( boxes[j].contains( new google.maps.LatLng( parseFloat(  gasolineras[i]['Longitud']   ) , parseFloat( gasolineras[i]['Latitud'] ) ) ) )
	        {
	            

            if( gasolineras[i][campoCarburanteBarato] != undefined && ( parseFloat( gasolineras[i][campoCarburanteBarato].replace(',','.') ) < carburanteBarato )){
              idCarburanteBarato = i;
              carburanteBarato = parseFloat( gasolineras[i][campoCarburanteBarato].replace(',','.') );
            }

            marker = new google.maps.Marker({
              position: new google.maps.LatLng( parseFloat(  gasolineras[i]['Longitud']   ) , parseFloat( gasolineras[i]['Latitud'] ) ),
              map: map,
              icon: 'logos/'+gasolineras[i]['icon'] ,
              title: gasolineras[i]['Id']+''
	          });

            markersArray.push( marker );
            google.maps.event.addListener( marker, 'click' , (function(marker, i) {

              return function(){


                  $("#idgas95").html(gasolineras[i]['Gasolina95']);
                  $("#idgas95p").html(gasolineras[i]['Gasolina95Proteccion']);

                  $("#idgas98").html(gasolineras[i]['Gasolina98']);
                  $("#idgasdiesel").html(gasolineras[i]['GasoleoA']);
                  $("#idgasdieselm").html(gasolineras[i]['GasoleoNuevo']);
                  $("#idgasbombonas").html(gasolineras[i]['GasesLicuados']);

                  $("#buttoncomollegar").attr("lat",gasolineras[i]['Longitud']);
                  $("#buttoncomollegar").attr("lan",gasolineras[i]['Latitud']);

                  $("#idgasnombre").html('<img width="24" src="logos/'+gasolineras[i]['icon']+'" />&nbsp;'+gasolineras[i]['Rotulo']);

                  $("#idgasdireccion").html(gasolineras[i]['Direccion']);
                  $("#idgasmunicipio").html(gasolineras[i]['Municipio']);
                  $("#idgaslocalidad").html(gasolineras[i]['Localidad']);
                  $("#idgascp").html(gasolineras[i]['CP']);
                  $("#idgasmargen").html(gasolineras[i]['Margen']);
                  $("#idgasmunicipio").html(gasolineras[i]['Municipio']);
                  $("#idgasventa").html(gasolineras[i]['TipodeVenta']);

                  $("#info-gasolinera").show();

                /*contenido = "<b>"+gasolineras[i]['Rotulo']+"</b><br />";
                
                if( show95 && show95p && gasolineras[i]['Gasolina95'] != undefined )
                  contenido += '<span class="gasolina95"><b>Gasolina 95:</b> '+gasolineras[i]['Gasolina95']+'&euro;/litro</span><br />';
                else{
                  contenido += (( show95 == true && gasolineras[i]['Gasolina95'] != undefined  )?('<span class="gasolina95"><b>Gasolina 95:</b> '+gasolineras[i]['Gasolina95']+'&euro;/litro</span><br />'):(''));
                  contenido += (( show95p == true && gasolineras[i]['Gasolina95Proteccion'] != undefined  )?('<span class="gasolina95p"><b>Gasolina 95 Protección:</b> '+gasolineras[i]['Gasolina95Proteccion']+'&euro;/litro</span><br />'):(''));
                }

                contenido += (( show98 == true && gasolineras[i]['Gasolina98'] != undefined  )?('<span class="gasolina98"><b>Gasolina 98:</b> '+gasolineras[i]['Gasolina98']+'&euro;/litro</span><br />'):(''));
                contenido += (( showGasoilA == true && gasolineras[i]['GasoleoA'] != undefined  )?('<span class="gasoleoa"><b>Diesel:</b> '+gasolineras[i]['GasoleoA']+'&euro;/litro</span><br />'):(''));
                contenido += (( showGasoilN == true && gasolineras[i]['GasoleoNuevo'] != undefined )?('<span class="gasoleon"><b>Gasoil:</b> '+gasolineras[i]['GasoleoNuevo']+'&euro;/litro</span><br />'):(''));
                contenido += (( showLicuados == true && gasolineras[i]['GasesLicuados'] != undefined )?('<span class="licuados"><b>Bombonas Disa: </b> '+gasolineras[i]['GasesLicuados']+'&euro;/litro</span><br />'):(''));
                contenido += '<hr><button class="ui-btn ui-btn-b comollegar ui-mini" lat="'+gasolineras[i]['Longitud']+'" lan="'+parseFloat( gasolineras[i]['Latitud'] )+'">Cómo llegar</button>';

                infowindow.setContent( contenido );
                infowindow.open(map, marker);*/
                
              }       

            })(marker, i));

	        }
	      }
	    }

      masBaratas = [];
      for( j = 0 ; j < boxes.length ; j++ ){  
        for( i = 0 ; i < gasolineras.length ; i++ ){
        
          if( boxes[j].contains( new google.maps.LatLng( parseFloat(  gasolineras[i]['Longitud']   ) , parseFloat( gasolineras[i]['Latitud'] ) ) ) ){

            if( gasolineras[i][campoCarburanteBarato] != undefined && ( parseFloat( gasolineras[i][campoCarburanteBarato].replace(',','.') ) == carburanteBarato )){

              if( masBaratas.indexOf( i ) == -1 ){
              
                if( gasolineras[i]['icon'] == 'disa.png' && showDisa == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'repsol.png' && showRepsol == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'tgas.png' && showTgas == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'shell.png' && showShell == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'bp.png' && showBp == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'pcan.png' && showPcan == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'cepsa.png' && showCepsa == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'texaco.png' && showTexaco == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'unknown.png' && showOtras == 0 )
                  continue;

                masBaratas.push( i );

              }

            }
          }
        }
      }

      for( m = 0 ; m < masBaratas.length ; m++ )
      {



        idCarburanteBarato = masBaratas[m];

        if( $("#toggleDestacarBarata").is(':checked') )
        {
         
          for( i = 0 ; i < markersArray.length ; i++ )
            if( markersArray[i]['title'] == gasolineras[idCarburanteBarato]['Id'] )
              markersArray[i].setMap(null);

          if( idCarburanteBarato != -1 ){
            marker = new google.maps.Marker({
              position: new google.maps.LatLng( parseFloat(  gasolineras[idCarburanteBarato]['Longitud']   ) , parseFloat( gasolineras[idCarburanteBarato]['Latitud'] ) ),
              map: map,
              icon: 'logos/mejor.png'
            });
          }

          markersArray.push( marker );
          google.maps.event.addListener( marker, 'click' , (function(marker, idCarburanteBarato) {

            return function(){

                  $("#idgas95").html(gasolineras[idCarburanteBarato]['Gasolina95']);
                  $("#idgas95p").html(gasolineras[idCarburanteBarato]['Gasolina95Proteccion']);

                  $("#idgas98").html(gasolineras[idCarburanteBarato]['Gasolina98']);
                  $("#idgasdiesel").html(gasolineras[idCarburanteBarato]['GasoleoA']);
                  $("#idgasdieselm").html(gasolineras[idCarburanteBarato]['GasoleoNuevo']);
                  $("#idgasbombonas").html(gasolineras[idCarburanteBarato]['GasesLicuados']);

                  $("#buttoncomollegar").attr("lat",gasolineras[idCarburanteBarato]['Longitud']);
                  $("#buttoncomollegar").attr("lan",gasolineras[idCarburanteBarato]['Latitud']);


                  $("#idgasnombre").html('<img width="24" src="logos/'+gasolineras[idCarburanteBarato]['icon']+'" />&nbsp;'+gasolineras[idCarburanteBarato]['Rotulo']);

                  $("#idgasdireccion").html(gasolineras[idCarburanteBarato]['Direccion']);
                  $("#idgasmunicipio").html(gasolineras[idCarburanteBarato]['Municipio']);
                  $("#idgaslocalidad").html(gasolineras[idCarburanteBarato]['Localidad']);
                  $("#idgascp").html(gasolineras[idCarburanteBarato]['CP']);
                  $("#idgasmargen").html(gasolineras[idCarburanteBarato]['Margen']);
                  $("#idgasmunicipio").html(gasolineras[idCarburanteBarato]['Municipio']);
                  $("#idgasventa").html(gasolineras[idCarburanteBarato]['TipodeVenta']);

                  $("#info-gasolinera").show();

              /*contenido = "<b>"+gasolineras[idCarburanteBarato]['Rotulo']+"</b><br />";
              contenido += '<span class="gasolina95"><b>Precio:</b> '+gasolineras[idCarburanteBarato][campoCarburanteBarato]+'&euro;/litro</span><br />';
              contenido += '<hr><button class="ui-btn ui-btn-b comollegar ui-mini" lat="'+gasolineras[idCarburanteBarato]['Longitud']+'" lan="'+parseFloat( gasolineras[idCarburanteBarato]['Latitud'] )+'">Cómo llegar</button>';

              infowindow.setContent( contenido );
              infowindow.open( map, marker);*/
        
            }       
          })(marker, idCarburanteBarato));
        
        }
      
      }

      clearBoxes();

    }
    
    //----------------------------------------

    function clearBoxes() {

      if( circle != null )
        circle.setMap(null);

      if (boxpolys != null) {
        for (var i = 0; i < boxpolys.length; i++) {
          boxpolys[i].setMap(null);
        }
      }
      boxpolys = null;
    }

    //----------------------------------------

    function getCercanas( p )
    {

        if( p == undefined )
          if( markersArray == null || marker == null )
            return void(0);

        $("#gasolineras").addClass('ui-disabled');
        $("#configuracion").addClass('ui-disabled');

        clearBoxes();

        if( gasolineras == undefined )
          loadData();

        if( p == undefined )
          marker.setMap(null);

        if( markersArray != null ){
    
          for (var i = 0; i < markersArray.length; i++ ) {
            markersArray[i].setMap(null);
          }
          markersArray.length = 0;
    
        }

        tmpZoom = map.getZoom();
        map.setZoom( tmpZoom-2 );
        map.setZoom( tmpZoom );

        if( isNaN( $("#desvio").val() ) )
          $("#desvio").val(500);

        if( p == undefined ){

          var populationOptions = {
            strokeColor: '#00B3C3',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#00B3C3',
            fillOpacity: 0.35,
            map: map,
            center: new google.maps.LatLng( routes[0].location.lat() , routes[0].location.lng() ),
            radius: parseInt($("#desvio").val())  // en metros
          };

          map.panTo( new google.maps.LatLng( routes[0].location.lat() , routes[0].location.lng() ));

        }else{

          var populationOptions = {
            strokeColor: '#80FF00',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#80FF00',
            fillOpacity: 0.35,
            map: map,
            center: new google.maps.LatLng( p.k , p.B ),
            radius: parseInt( $("#desvio").val() )  // en metros
          };

          map.panTo( new google.maps.LatLng( p.k , p.B ));

        }

        map.setZoom( 15 );
        circle = new google.maps.Circle( populationOptions );

        carburanteBarato = 99.99; // Este es el que le pongo a mi coche
        idCarburanteBarato = -1;
        campoCarburanteBarato = $("#filterby").val();




        for( i = 0 ; i < gasolineras.length ; i++ ){

          if( gasolineras[i]['icon'] == 'disa.png' && showDisa == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'repsol.png' && showRepsol == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'tgas.png' && showTgas == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'shell.png' && showShell == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'bp.png' && showBp == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'pcan.png' && showPcan == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'cepsa.png' && showCepsa == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'texaco.png' && showTexaco == 0 )
            continue;
          if( gasolineras[i]['icon'] == 'unknown.png' && showOtras == 0 )
            continue;


          if( circle.contains( new google.maps.LatLng( parseFloat(  gasolineras[i]['Longitud']   ) , parseFloat( gasolineras[i]['Latitud'] ) ) ) )
          {   

              if( gasolineras[i][campoCarburanteBarato] != undefined && ( parseFloat( gasolineras[i][campoCarburanteBarato].replace(',','.') ) < carburanteBarato )){
                idCarburanteBarato = i;
                carburanteBarato = parseFloat( gasolineras[i][campoCarburanteBarato].replace(',','.') );
              }

              marker = new google.maps.Marker({
                position: new google.maps.LatLng( parseFloat(  gasolineras[i]['Longitud']   ) , parseFloat( gasolineras[i]['Latitud'] ) ),
                map: map,
                icon: 'logos/'+gasolineras[i]['icon']
              });

              markersArray.push( marker );
              google.maps.event.addListener( marker, 'click' , (function(marker, i) {

                return function() {

                  /*contenido = "<b>"+gasolineras[i]['Rotulo']+"</b><br />";
                  
                  if( show95 && show95p && gasolineras[i]['Gasolina95'] != undefined )
                    contenido += '<span class="gasolina95"><b>Gasolina 95:</b> '+gasolineras[i]['Gasolina95']+'&euro;/litro</span><br />';
                  else{
                    contenido += (( show95 == true && gasolineras[i]['Gasolina95'] != undefined  )?('<span class="gasolina95"><b>Gasolina 95:</b> '+gasolineras[i]['Gasolina95']+'&euro;/litro</span><br />'):(''));
                    contenido += (( show95p == true && gasolineras[i]['Gasolina95Proteccion'] != undefined  )?('<span class="gasolina95p"><b>Gasolina 95 Protección:</b> '+gasolineras[i]['Gasolina95Proteccion']+'&euro;/litro</span><br />'):(''));
                  }

                  contenido += (( show98 == true && gasolineras[i]['Gasolina98'] != undefined  )?('<span class="gasolina98"><b>Gasolina 98:</b> '+gasolineras[i]['Gasolina98']+'&euro;/litro</span><br />'):(''));
                  contenido += (( showGasoilA == true && gasolineras[i]['GasoleoA'] != undefined  )?('<span class="gasoleoa"><b>Diesel:</b> '+gasolineras[i]['GasoleoA']+'&euro;/litro</span><br />'):(''));
                  contenido += (( showGasoilN == true && gasolineras[i]['GasoleoNuevo'] != undefined )?('<span class="gasoleon"><b>Gasoil:</b> '+gasolineras[i]['GasoleoNuevo']+'&euro;/litro</span><br />'):(''));
                  contenido += (( showLicuados == true && gasolineras[i]['GasesLicuados'] != undefined )?('<span class="licuados"><b>Bombonas Disa: </b> '+gasolineras[i]['GasesLicuados']+'&euro;/litro</span><br />'):(''));
                  contenido += '<hr><button class="ui-btn ui-btn-b comollegar ui-mini" lat="'+gasolineras[i]['Longitud']+'" lan="'+parseFloat( gasolineras[i]['Latitud'] )+'">Cómo llegar</button>';

                  infowindow.setContent( contenido );
                  infowindow.open(map, marker);*/


                  $("#idgas95").html(gasolineras[i]['Gasolina95']);
                  $("#idgas95p").html(gasolineras[i]['Gasolina95Proteccion']);

                  $("#idgas98").html(gasolineras[i]['Gasolina98']);
                  $("#idgasdiesel").html(gasolineras[i]['GasoleoA']);
                  $("#idgasdieselm").html(gasolineras[i]['GasoleoNuevo']);
                  $("#idgasbombonas").html(gasolineras[i]['GasesLicuados']);

                  $("#buttoncomollegar").attr("lat",gasolineras[i]['Longitud']);
                  $("#buttoncomollegar").attr("lan",gasolineras[i]['Latitud']);

                  $("#idgasnombre").html('<img width="24" src="logos/'+gasolineras[i]['icon']+'" />&nbsp;'+gasolineras[i]['Rotulo']);

                  $("#idgasdireccion").html(gasolineras[i]['Direccion']);
                  $("#idgasmunicipio").html(gasolineras[i]['Municipio']);
                  $("#idgaslocalidad").html(gasolineras[i]['Localidad']);
                  $("#idgascp").html(gasolineras[i]['CP']);
                  $("#idgasmargen").html(gasolineras[i]['Margen']);
                  $("#idgasmunicipio").html(gasolineras[i]['Municipio']);
                  $("#idgasventa").html(gasolineras[i]['TipodeVenta']);

                  $("#info-gasolinera").show();
                
                }

              })(marker, i));
          
          }

        }


      masBaratas = [];
        for( i = 0 ; i < gasolineras.length ; i++ ){
        
          if( circle.contains( new google.maps.LatLng( parseFloat(  gasolineras[i]['Longitud']   ) , parseFloat( gasolineras[i]['Latitud'] ) ) ) ){

            if( gasolineras[i][campoCarburanteBarato] != undefined && ( parseFloat( gasolineras[i][campoCarburanteBarato].replace(',','.') ) == carburanteBarato )){

              if( masBaratas.indexOf( i ) == -1 ){
               

                if( gasolineras[i]['icon'] == 'disa.png' && showDisa == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'repsol.png' && showRepsol == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'tgas.png' && showTgas == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'shell.png' && showShell == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'bp.png' && showBp == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'pcan.png' && showPcan == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'cepsa.png' && showCepsa == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'texaco.png' && showTexaco == 0 )
                  continue;
                if( gasolineras[i]['icon'] == 'unknown.png' && showOtras == 0 )
                  continue;

                masBaratas.push( i );
              }

            }
          }
        }

      for( m = 0 ; m < masBaratas.length ; m++ )
      {

        idCarburanteBarato = masBaratas[m];



        if( $("#toggleDestacarBarata").is(':checked') )
        {

         
          for( i = 0 ; i < markersArray.length ; i++ ){
            
            if( 
              parseFloat( markersArray[i].position.B ).toFixed( 5 ) == parseFloat( gasolineras[masBaratas[m]].Latitud ).toFixed( 5 ) 
              &&
              parseFloat( markersArray[i].position.k ).toFixed( 5 ) == parseFloat( gasolineras[masBaratas[m]].Longitud ).toFixed( 5 ) 
              )
            {

              markersArray[i].setMap(null);

            }

          }

          if( idCarburanteBarato != -1 ){

            marker = new google.maps.Marker({
              position: new google.maps.LatLng( parseFloat(  gasolineras[idCarburanteBarato]['Longitud']   ) , parseFloat( gasolineras[idCarburanteBarato]['Latitud'] ) ),
              map: map, 
              icon: 'logos/mejor.png'
            });
          }

          markersArray.push( marker );
          google.maps.event.addListener( marker, 'click' , (function(marker, idCarburanteBarato) {

            return function(){

                  $("#idgas95").html(gasolineras[idCarburanteBarato]['Gasolina95']);
                  $("#idgas95p").html(gasolineras[idCarburanteBarato]['Gasolina95Proteccion']);

                  $("#idgas98").html(gasolineras[idCarburanteBarato]['Gasolina98']);
                  $("#idgasdiesel").html(gasolineras[idCarburanteBarato]['GasoleoA']);
                  $("#idgasdieselm").html(gasolineras[idCarburanteBarato]['GasoleoNuevo']);
                  $("#idgasbombonas").html(gasolineras[idCarburanteBarato]['GasesLicuados']);

                  $("#buttoncomollegar").attr("lat",gasolineras[idCarburanteBarato]['Longitud']);
                  $("#buttoncomollegar").attr("lan",gasolineras[i]['Latitud']);

                  $("#idgasnombre").html('<img width="24" src="logos/'+gasolineras[idCarburanteBarato]['icon']+'" />&nbsp;'+gasolineras[idCarburanteBarato]['Rotulo']);

                  $("#idgasdireccion").html(gasolineras[idCarburanteBarato]['Direccion']);
                  $("#idgasmunicipio").html(gasolineras[idCarburanteBarato]['Municipio']);
                  $("#idgaslocalidad").html(gasolineras[idCarburanteBarato]['Localidad']);
                  $("#idgascp").html(gasolineras[idCarburanteBarato]['CP']);
                  $("#idgasmargen").html(gasolineras[idCarburanteBarato]['Margen']);
                  $("#idgasmunicipio").html(gasolineras[idCarburanteBarato]['Municipio']);
                  $("#idgasventa").html(gasolineras[idCarburanteBarato]['TipodeVenta']);

                  $("#info-gasolinera").show();

              /*contenido = "<b>"+gasolineras[idCarburanteBarato]['Rotulo']+"</b><br />";
              contenido += '<span class="gasolina95"><b>Precio:</b> '+gasolineras[idCarburanteBarato][campoCarburanteBarato]+'&euro;/litro</span><br />';
              contenido += '<hr><button class="ui-btn ui-btn-b comollegar ui-mini" lat="'+gasolineras[idCarburanteBarato]['Longitud']+'" lan="'+parseFloat( gasolineras[idCarburanteBarato]['Latitud'] )+'">Cómo llegar</button>';

              infowindow.setContent( contenido );
              infowindow.open( map, marker);*/
        
            }       
          })(marker, idCarburanteBarato));
        
        }
      
      }

      routes = [];

    }

  
  //----------------------------------------

  function desactivarMenu(){
        
    $("#gasolineras").removeClass('ui-disabled');
    $("#configuracion").removeClass('ui-disabled');
    $("#cercanas").removeClass('ui-disabled');
    $("#cancelButton_li").remove();

    clearBoxes();
        
    if( marker != undefined )
      marker.setMap(null);

    if( routesPin != null ){
      for (var i = 0; i < routesPin.length; i++ ) {
        routesPin[i].setMap(null);
      }
      routesPin.length = 0;
    }
    routes = [];


    if( markersArray != null ){
      for (var i = 0; i < markersArray.length; i++ ) {
        markersArray[i].setMap(null);
      }
      markersArray.length = 0;
    }

    if( markerCameraArray != null ){
      for (var i = 0; i < markerCameraArray.length; i++ ) {
        markerCameraArray[i].setMap(null);
      }
      markerCameraArray.length = 0;
    }

    if( markerCartelArray != null ){
      for (var i = 0; i < markerCartelArray.length; i++ ) {
        markerCartelArray[i].setMap(null);
      }
      markerCartelArray.length = 0;
    }

    if( markerDensidadArray != null ){
      for (var i = 0; i < markerDensidadArray.length; i++ ) {
        markerDensidadArray[i].setMap(null);
      }
      markerDensidadArray.length = 0;
    }

    if( markerIncidencesArray != null ){
      for (var i = 0; i < markerIncidencesArray.length; i++ ) {
        markerIncidencesArray[i].setMap(null);
      }
      markerIncidencesArray.length = 0;
    }

  }

  //----------------------------------------


  function cleanMap(){
    
    desactivarMenu();
    directionsRenderer.setMap(null);
    directionService = new google.maps.DirectionsService();
    directionsRenderer = new google.maps.DirectionsRenderer({ map: map , suppressMarkers : true });

    $("#toggleCamera").prop('checked',false);
    $("#toggleDensidad").prop('checked',false);
    $("#toggleCartel").prop('checked',false);
    $("#toggleIncidences").prop('checked',false);;

    var tmpZoom = 0;
    tmpZoom = map.getZoom();

    window.setTimeout(function() {
          map.setZoom( tmpZoom-2 );
          map.setZoom( tmpZoom );
        }, 500 );

  }

  //----------------------------------------

  function showIncidences(){

    $.ajax({

      url: serverUrl+'?action=incidencias',
      global: true,
      async: true,
      type: "GET",         
      dataType: 'json',
      success: function( data )
      {
                  
        for( i = 0 ; i < data.length ; i++ )
        {

          markerIncidences = new google.maps.Marker({
            position: new google.maps.LatLng( parseFloat(  data[i]['lati']   ) , parseFloat( data[i]['lon'] )),
            map: map,
            icon: 'images/incidence.png'
          });

          markerIncidencesArray.push(markerIncidences);
          
              parameters = data[i];

          google.maps.event.addListener( markerIncidences, 'click' , (function(markerIncidences, i) {
            return function() {
              infowindow.setContent( '<h3>'+data[i]['parameters'][1]['value']+' ('+data[i]['descInciType']+')</h3>'+data[i]['parameters'][0]['value']+" - "+data[i]['parameters'][3]['value'] );
              infowindow.open(map, markerIncidences);
            }
          })(markerIncidences, i));


        }
        
        $.unblockUI();

      }

    });

  }

  function showRoadInfo(){

    $.ajax({

      url: serverUrl+'?action=info',
      global: true,
      async: true,
      type: "GET",         
      dataType: 'json',
      success: function( data )
      {
                  
        for( i = 0 ; i < data.length ; i++ )
        {

          markerDensidad = new google.maps.Marker({
            position: new google.maps.LatLng( parseFloat(  data[i]['realLat']   ) , parseFloat( data[i]['realLon'] )),
            map: map,
            icon: 'images/road.png'
          });

          markerDensidadArray.push(markerDensidad);
          
          google.maps.event.addListener( markerDensidad, 'click' , (function(markerDensidad, i) {
            return function() {
              infowindow.setContent( '<b>'+data[i]['title']+'</b>'+data[i]['paramsHtml'] );
              infowindow.open(map, markerDensidad);
            }
          })(markerDensidad, i));


        }
        
        $.unblockUI();

      }

    });

  }


  //----------------------------------------


  function showCartelInfo(){
    
    directionsRenderer.setMap(null);
    $.ajax({

      url: serverUrl+'?action=carteles',
      global: true,
      async: true,
      type: "GET",         
      dataType: 'json',
      success: function( data )
      {
                
        for( i = 0 ; i < data.length ; i++ )
        {

          markerCartel = new google.maps.Marker({
            position: new google.maps.LatLng( parseFloat(  data[i]['realLat'] ) , parseFloat( data[i]['realLon'] ) ),
            map: map,
            icon: 'images/sign.png'
          });

          markerCartelArray.push( markerCartel );   
          google.maps.event.addListener( markerCartel, 'click' , (function(markerCartel, i) {
            
            return function() {

              separador = data[i]['accion'].length/2;
              auxCartel = "";
              
              for( j = 0 ; j < separador ; j++ )
                auxCartel += '<img src="'+data[i]['accion'][j]+'" /> '+data[i]['accion'][j+separador]+"<br />";

              infowindow.setContent( auxCartel );            
              infowindow.open(map, markerCartel);
            
            }

          })(markerCartel, i));

        }

        $.unblockUI();

      } 

    });

  }

  //----------------------------------------

  function showCamarasInfo(){
    
    directionsRenderer.setMap(null);
    $.ajax({

      url: serverUrl+'?action=camaras',
      global: true,
      async: true,
      type: "GET",         
      dataType: 'json',
      success: function( data )
      {
        for( i = 0 ; i < data.length ; i++ )
        {
          
          markerCamera = new google.maps.Marker({
            position: new google.maps.LatLng( parseFloat(  data[i][5] ) , parseFloat( data[i][4] ) ),
            map: map,
            icon: 'images/camera.png'
          });

          markerCameraArray.push( markerCamera );
               
          google.maps.event.addListener( markerCamera, 'click' , (function(markerCamera, i) {
            return function() {

              infowindow.setContent( '<img src="'+data[i][3]+'?'+Math.random()+'" style="width: inherit; max-width: 200px; margin: 10px;" /> ' );                        
              infowindow.open(map, markerCamera);
                        
            }
          })( markerCamera , i ));

        }
        $.unblockUI();          
      },
      error: function( data ){
        console.log('Error: '+data);
      }

    });

  }

});

